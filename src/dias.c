#include<stdio.h>
#include<math.h> 
void transformarDias(double dias){
	double anios=dias/360;
	double parteDecimal,meses;
	parteDecimal=modf(anios,&anios);
	meses=parteDecimal*(360/30);
	parteDecimal=modf(meses,&meses);
	parteDecimal=modf(parteDecimal*30,&dias);

	printf("Years\tMonth\tDays\n");
	printf("%.0f\t%.0f\t%.0f\n",anios,meses,dias);
	
}
