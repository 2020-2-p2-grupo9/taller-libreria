#include <stdio.h>
#include <math.h>
void transformarSegundos(double segundos){
double horas=segundos/3600;
	double parteDecimal,minutos;
	parteDecimal=modf(horas,&horas);
	minutos=parteDecimal*(60);
	parteDecimal=modf(minutos,&minutos);
	parteDecimal=modf(parteDecimal*60,&segundos);

	printf("Hours\tMinutes\tSeconds\n");
	printf("%.0f\t%.0f\t%.0f\n",horas,minutos,segundos);
}
