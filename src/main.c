#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "utilfecha.h"

int main(int argc,char**argv ){
double numDias;
double numSegundos;
char opciones;
while((opciones=getopt(argc,argv,"s:d:f:"))!=-1){
switch(opciones){
	case 'd':
		numDias= atof(optarg);
		transformarDias(numDias);
		break;
	case 's':
		numSegundos= atof(optarg);
		transformarSegundos(numSegundos);
		break;
	case 'f':
		cambiarFormato(optarg);
		break;	
}
}
}
