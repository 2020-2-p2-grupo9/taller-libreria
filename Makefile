CC=gcc -c -Wall
all: bin/utilfechaEstatico bin/utilfechaDinamico
bin/utilfechaEstatico: lib/libtime.a obj/main.o 
	gcc -static obj/main.o -L lib/ -ltime -o $@

bin/utilfechaDinamico: lib/libtime.so obj/main.o
	gcc obj/main.o -L lib/ -ltime -o $@
	
lib/libtime.so: src/dias.c src/formato.c src/segundos.c
	gcc -fPIC -shared src/dias.c src/formato.c src/segundos.c -o $@

lib/libtime.a: obj/dias.o obj/formato.o obj/segundos.o
	ar rcs $@ obj/dias.o obj/formato.o obj/segundos.o

obj/dias.o: src/dias.c 
	$(CC) src/dias.c -o $@

obj/formato.o: src/formato.c
	$(CC) src/formato.c -o $@

obj/segundos.o: src/segundos.c
	$(CC) src/segundos.c -o $@

obj/main.o: src/main.c
	$(CC) -I include/ src/main.c -o $@

clean:
	rm bin/* obj/* lib/*
